package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"net/http"

	_ "github.com/lib/pq"
)

const portNumber = ":8025"

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "3971"
	dbname   = "film_db"
)

func main() {
	psqlconn := fmt.Sprintf("host= %s port= %d user= %s password= %s dbname= %s sslmode=disable", host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlconn)
	CheckError(err)

	defer db.Close()

	insertStmt := `insert into "User"("Name", "UserId") values('Asad',01)`
	_, e := db.Exec(insertStmt)
	CheckError(e)

	insertDynStmt := `insert into "User" ("Name", "UserId") values($1, $2)`
	_, e = db.Exec(insertDynStmt, "Said", 02)

	http.HandleFunc("/", handler)
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("assets"))))

	http.ListenAndServe(portNumber, nil)
	// http.ListenAndServeTLS("", "cert.pem", "key.pem", nil)
}

func CheckError(err error) {
	if err != nil {
		panic(err)
	}
}

func Home(w http.ResponseWriter, r *http.Request) {
	var filename = "home.html"
	t, err := template.ParseFiles(filename)
	if err != nil {
		fmt.Println("Error when parsing a file | ", err)
		return
	}
	err = t.ExecuteTemplate(w, filename, nil)
	if err != nil {
		fmt.Println("Error when executing the template | ", err)
		return
	}
}

func Signup(w http.ResponseWriter, r *http.Request) {
	var filename = "signup.html"
	t, err := template.ParseFiles(filename)
	if err != nil {
		fmt.Println("Error when parsing a file | ", err)
		return
	}
	err = t.ExecuteTemplate(w, filename, nil)
	if err != nil {
		fmt.Println("Error when executing the template | ", err)
		return
	}
}

func Login(w http.ResponseWriter, r *http.Request) {
	var filename = "login.html"
	t, err := template.ParseFiles(filename)
	if err != nil {
		fmt.Println("Error when parsing a file | ", err)
		return
	}
	err = t.ExecuteTemplate(w, filename, "Please, Login")
	if err != nil {
		fmt.Println("Error when executing the template | ", err)
		return
	}
}

var userDB = map[string]string{
	"Asad": "badPassword",
}

func LoginSubmit(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	password := r.FormValue("password")

	if userDB[username] == password {
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "You have now logged in!")
	} else {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "Not matched!")
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/":
		Home(w, r)
	case "/signup":
		Signup(w, r)
	case "/login":
		Login(w, r)
	case "/login-submit":
		LoginSubmit(w, r)
	default:
		fmt.Fprintf(w, "Whazap")
	}

}

// func renderTemplate(w http.ResponseWriter, templateName string) {
// 	parsedTemplate, errTmp := template.ParseFiles("./pages/" + templateName)
// 	if errTmp != nil {
// 		fmt.Println("Error template parsing", errTmp)
// 		return
// 	}
// 	resolvedTemplate, errLay := parsedTemplate.ParseFiles("./layouts/base.layout.tpl")
// 	if errLay != nil {
// 		fmt.Println("Error layout parsing", errLay)
// 		return
// 	}

// 	errExe := resolvedTemplate.Execute(w, nil)
// 	if errExe != nil {
// 		fmt.Println("error executing template:", errExe)
// 	}
// }
